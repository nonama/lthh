from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from models import Profile, Area, Citizenship, DocumentType, Metro
from forms import ProfileForm
import lthh.settings as settings
import os, re, base64, tempfile
from django.core.files import File
from django.core import serializers
from django.forms.models import model_to_dict
import json as simplejson


@login_required
def profile(request):
    p = get_object_or_404(Profile, user=request.user)
    reg = Area.objects.all()
    ctz = Citizenship.objects.all()
    if request.method == 'POST':
        profile_form = ProfileForm(request.POST)
        if profile_form.is_valid():
            profile = profile_form.save(commit=False)
            profile.user = p.user
            profile.filled = True
            profile.badge_id = p.badge_id
            profile.status = p.status
            profile.save()
            return JsonResponse({'status':'saved'})
    #else:
        #profile_form = ProfileForm(instance = Profile.objects.get(user_id=request.user.id))
    #return JsonResponse({'status':'loaded'})

@login_required
def edit(request):
    p = get_object_or_404(Profile, user=request.user)
    reg = Area.objects.all()
    ctz = Citizenship.objects.all()
    doc = DocumentType.objects.all()
    if request.method == 'POST':
        dt = 0
        profile_form = ProfileForm(request.POST)
        if profile_form.is_valid():
            profile = profile_form.save(commit=False)
            if request.POST.get('u_avatar'):
                dt = 1
                sp = os.path.join(settings.MEDIA_ROOT, 'avatars', request.user.username + "_avatar.jpg")
                dataUrlPattern = re.compile('data:image/(png|jpeg);base64,(.*)$')
                ImageData = request.POST.get('u_avatar')
                ImageData = dataUrlPattern.match(ImageData).group(2)
                ImageData = base64.b64decode(ImageData)
                ava = tempfile.TemporaryFile()
                av = File(ava)
                av.write(ImageData)
                av.name = request.user.username + "_avatar.jpg"
                profile.avatar = av
            else:
                profile.avatar = p.avatar
            profile.user = p.user
            if (request.POST.get('f_name')
                and request.POST.get('l_name')
                and request.POST.get('bd_day')
                and request.POST.get('bd_month')
                and request.POST.get('bd_year')
                and request.POST.get('phone')):
                profile.filled = True
            else:
                profile.filled = False
            profile.badge_id = p.badge_id
            profile.status = p.status
            profile.save()
            if dt == 1:
                ava.close()
            return HttpResponseRedirect('jobs')
    else:
        profile_form = ProfileForm(instance = Profile.objects.get(user_id=request.user.id))
    return render(request, 'edit.html', {'profile_form' : profile_form, 'region' : reg, 'citiz' : ctz, 'doc' : doc})

def metro_list(request, regid):
    if request.is_ajax():
        regions = Metro.objects.distinct('area')
        metros = Metro.objects.filter(area=regid).order_by('name')
        mlist = []
        for m in metros:
            metro = {}
            metro['id'] = m.id
            metro['name'] = m.name
            mlist.append(metro)
        out = simplejson.dumps(mlist)
        return HttpResponse(out, content_type="application/json")