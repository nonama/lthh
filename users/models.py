# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from datetime import datetime
from storage import FSS

from allauth.account.signals import user_signed_up
from django.dispatch import receiver

from dbmail import send_db_sms, send_db_mail


store = FSS()

MONTHS = (
    ('01', 'Январь'),
    ('02', 'Февраль'),
    ('03', 'Март'),
    ('04', 'Апрель'),
    ('05', 'Май'),
    ('06', 'Июнь'),
    ('07', 'Июль'),
    ('08', 'Август'),
    ('09', 'Сентябрь'),
    ('10', 'Октябрь'),
    ('11', 'Ноябрь'),
    ('12', 'Декабрь'),
)

FAMILY = (
    ('yes', "Женат\Замужем"),
    ('no', "Холост\Не замужем")
)


class Area(models.Model):
    id = models.IntegerField(unique=True, null=False, primary_key=True)
    parent_id = models.IntegerField(null=True)
    area = models.CharField(max_length=255)
    
    def __unicode__(self):
        return self.area
    
    class Meta:
        ordering = ('area',)
        
class Metro(models.Model):
    id = models.IntegerField(unique=True, null=False, primary_key=True)
    name = models.CharField(null=True, max_length=255)
    area = models.ForeignKey(Area)
    
    def __unicode__(self):
        return self.name


class Citizenship(models.Model):
    id = models.IntegerField(unique=True, null=False, primary_key=True)
    citizenship = models.CharField(max_length=255)
    
    def __unicode__(self):
        return self.citizenship
      
    class Meta:
        ordering = ('citizenship',)

class DocumentType(models.Model):
    id = models.IntegerField(unique=True, null=False, primary_key=True)
    document = models.CharField(max_length=255)
    
    def __unicode__(self):
        return self.document
    
    class Meta:
        ordering = ('document',)


class KnownFrom(models.Model):
    
    choice = models.CharField(max_length=255)
    
    def __unicode__(self):
        return self.choice


class Profile(models.Model):
    user = models.OneToOneField(User, primary_key=True)
    f_name = models.CharField(blank=True, max_length=30)
    m_name = models.CharField(blank=True, max_length=30)
    l_name = models.CharField(blank=True, max_length=30)
    bd_day = models.CharField(blank=True, max_length=2)
    bd_month = models.CharField(blank=True, choices=MONTHS, max_length=2)
    bd_year = models.CharField(blank=True, max_length=4)
    doc_type = models.ForeignKey(DocumentType, null=True, blank=True, default=1)
    pass_series = models.CharField(null=True, blank=True, max_length=20)
    pass_number = models.CharField(null=True, blank=True, max_length=20)
    issued_by = models.CharField(null=True, blank=True, max_length=255)
    issued_date = models.DateField(null=True, blank=True)
    sub_code = models.CharField(null=True, blank=True, max_length=10)
    birth_place = models.CharField(null=True, blank=True, max_length=255)
    citizenship = models.ForeignKey(Citizenship, null=True, blank=True)
    pass_reg_place = models.CharField(null=True, blank=True, max_length=255)
    curr_reg_place = models.CharField(null=True, blank=True, max_length=255)
    temp_reg = models.BooleanField(default=False)
    temp_reg_date = models.DateField(null=True, blank=True)
    temp_reg_last_date = models.DateField(null=True, blank=True)
    inn = models.BigIntegerField(null=True, blank=True)
    area = models.ForeignKey(Area, null=True, blank=True)
    metro = models.ForeignKey(Metro, null=True, blank=True)
    phone = models.CharField(null=True, blank=True, max_length=20)
    experience = models.TextField(null=True, blank=True)
    family = models.CharField(null=True, blank=True, choices=FAMILY, max_length=3)
    children = models.BooleanField(default=False)
    child_age_old = models.IntegerField(null=True, blank=True)
    child_age_jr = models.IntegerField(null=True, blank=True)
    web_edu = models.BooleanField(default=False)
    ru_card = models.BooleanField(default=False)
    med_book = models.BooleanField(default=False)
    mb_date = models.DateField(null=True, blank=True)
    mb_city = models.CharField(null=True, blank=True, max_length=25)
    med_exam_date = models.DateField(null=True, blank=True)
    mb_desc = models.CharField(null=True, blank=True, max_length=25)
    disability = models.BooleanField(default=False)
    disability_group = models.IntegerField(null=True, blank=True)
    infect = models.BooleanField(default=False)
    known_from_list = models.ForeignKey(KnownFrom, null=True, blank=True)
    known_from = models.TextField(null=True, blank=True)
    status = models.IntegerField(null=True, blank=True)
    badge_id = models.CharField(blank=True, null=True, max_length=25)
    avatar = models.ImageField(upload_to='avatars/', blank = True, default='/static/img/avatars/d_av.png', storage=store)
    filled = models.BooleanField(default=False)
    modified = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        return self.user.username
    
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)
            Notification.objects.create(user = instance, text = "Добро пожаловать в сообщество Лидер Тим! Регистрация прошла успешно!")

    post_save.connect(create_user_profile, sender=User)

from jobs.models import Notification
@receiver(user_signed_up, dispatch_uid="registration")
def user_signed_up_(request, user, **kwargs):
    user_phone = request.POST.get('phone')
    user_mail = request.POST.get('email')
    recipient = str(user_phone[0:2] + user_phone[4:7] + user_phone[9:12] + user_phone[13:15] + user_phone[16:18])
    send_db_sms(slug="reg_sms", recipient = recipient, use_celery=False)
    send_db_mail(slug="reg_mail", recipient = user_mail, use_celery=False)