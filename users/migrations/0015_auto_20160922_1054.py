# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0014_profile_experience'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='status',
            field=models.IntegerField(null=True),
        ),
        migrations.DeleteModel(
            name='UserStatus',
        ),
    ]
