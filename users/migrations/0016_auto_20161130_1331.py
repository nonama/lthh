# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0015_auto_20160922_1054'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='status',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
