# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0007_auto_20160524_1022'),
    ]

    operations = [
        migrations.CreateModel(
            name='Citizenship',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('citizenship', models.CharField(max_length=255)),
            ],
        ),
        migrations.AlterField(
            model_name='profile',
            name='citizenship',
            field=models.ForeignKey(blank=True, to='users.Citizenship', null=True),
        ),
    ]
