# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0013_auto_20160831_1542'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='experience',
            field=models.TextField(null=True, blank=True),
        ),
    ]
