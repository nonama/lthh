# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0012_auto_20160811_1113'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='doc_type',
            field=models.ForeignKey(default=1, blank=True, to='users.DocumentType', null=True),
        ),
    ]
