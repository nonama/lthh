# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0009_auto_20160707_1213'),
    ]

    operations = [
        migrations.CreateModel(
            name='Metro',
            fields=[
                ('id', models.IntegerField(unique=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=255, null=True)),
                ('area', models.ForeignKey(to='users.Area')),
            ],
        ),
        migrations.AddField(
            model_name='profile',
            name='metro',
            field=models.ForeignKey(blank=True, to='users.Metro', null=True),
        ),
    ]
