# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0011_auto_20160802_1121'),
    ]

    operations = [
        migrations.CreateModel(
            name='KnownFrom',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('choice', models.CharField(max_length=255)),
            ],
        ),
        migrations.AddField(
            model_name='profile',
            name='known_from_list',
            field=models.ForeignKey(blank=True, to='users.KnownFrom', null=True),
        ),
    ]
