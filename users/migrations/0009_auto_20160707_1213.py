# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0008_auto_20160705_1536'),
    ]

    operations = [
        migrations.CreateModel(
            name='DocumentType',
            fields=[
                ('id', models.IntegerField(unique=True, serialize=False, primary_key=True)),
                ('document', models.CharField(max_length=255)),
            ],
        ),
        migrations.AlterField(
            model_name='citizenship',
            name='id',
            field=models.IntegerField(unique=True, serialize=False, primary_key=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='pass_number',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='pass_series',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='profile',
            name='doc_type',
            field=models.ForeignKey(blank=True, to='users.DocumentType', null=True),
        ),
    ]
