# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0006_profile_badge_id'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='area',
            options={'ordering': ('area',)},
        ),
    ]
