# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0010_auto_20160707_1550'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='citizenship',
            options={'ordering': ('citizenship',)},
        ),
        migrations.AlterModelOptions(
            name='documenttype',
            options={'ordering': ('document',)},
        ),
    ]
