# -*- coding: utf-8 -*-
from django.contrib import admin
from users.models import Profile, Area, Citizenship, DocumentType, Metro, KnownFrom

class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'phone', 'f_name', 'l_name', 'modified', 'filled')
    search_fields = ('user__username', 'f_name', 'l_name', 'phone' )

admin.site.register(Profile, ProfileAdmin)
admin.site.register(Area)

admin.site.register(Citizenship)
admin.site.register(DocumentType)
admin.site.register(Metro)
admin.site.register(KnownFrom)