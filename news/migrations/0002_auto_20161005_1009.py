# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='new',
            name='published',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='new',
            name='content',
            field=ckeditor.fields.RichTextField(),
        ),
    ]
