# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='New',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, null=True, blank=True)),
                ('content', models.TextField()),
                ('date', models.DateField(auto_now_add=True)),
                ('thumbnail', models.FileField(default=b'/static/img/d_new.png', upload_to=b'news/', blank=True)),
            ],
        ),
    ]
