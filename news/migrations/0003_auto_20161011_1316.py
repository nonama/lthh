# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0002_auto_20161005_1009'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='new',
            options={'ordering': ('-date',)},
        ),
    ]
