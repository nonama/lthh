from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from forms import NewForm

from models import New

def news(request):
    news = New.objects.filter(published=True)
    return render(request, 'news.html', {'news' : news})

def new(request, nid):
    new = New.objects.get(id=nid)
    return render(request, 'new.html', {'new' : new})

#--------------------------------------------------------ADMIN PART---------------------------------------

def anews(request):
    news = New.objects.all()
    return render(request, 'admins/news.html', {'news' : news})

def anew(request, nid):
    new = New.objects.get(id=nid)
    return render(request, 'admins/new.html', {'new' : new})

def add_new(request):
    if request.method == 'POST':
        new_form = NewForm(request.POST, request.FILES)
        if new_form.is_valid():
            new_form.save()
        return HttpResponseRedirect('/masterzone/news')
    else:
        new_form = NewForm()
    return render(request, 'admins/add_new.html', {'new_form' : new_form})

def publish_new(request, nid):
    new = New.objects.get(id=nid)
    new.published = True
    new.save()
    return HttpResponseRedirect('/masterzone/news')

def edit_new(request, nid):
    new = New.objects.get(id=nid)
    if request.method == 'POST':
        new_form = NewForm(request.POST, request.FILES, instance=new)
        if new_form.is_valid():
            new_form.save()
        return HttpResponseRedirect('/masterzone/news')
    else:
        new_form = NewForm(instance=new)
    return render(request, 'admins/edit_new.html', {'new_form' : new_form})

def delete_new(request, nid):
    new = New.objects.filter(id=nid).delete()
    return HttpResponseRedirect('/masterzone/news')