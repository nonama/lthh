# -*- coding: utf-8 -*-
from django.db import models
from ckeditor.fields import RichTextField


class New(models.Model):
    title = models.CharField(null=True, blank=True, max_length=255)
    content = RichTextField()
    date = models.DateField(auto_now_add=True)
    thumbnail = models.FileField(upload_to='news/', blank = True, default='/static/img/d_new.png')
    published = models.BooleanField(default=False)
    
    def __unicode__(self):
        return self.title
    
    class Meta:
        ordering = ('-date',)