# -*- coding: utf-8 -*-
from django import forms
from models import New
from ckeditor.widgets import CKEditorWidget
from ckeditor_uploader.fields import RichTextUploadingField


class NewForm(forms.ModelForm):
    content = RichTextUploadingField('contents')
    class Meta:
        model = New
        exclude = ('id',)