# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from users.models import Area


class Subscribe(models.Model):
    user = models.ForeignKey(User)
    area = models.ForeignKey(Area)
    
    def __unicode__(self):
        return self.user.username
    
    class Meta:
        ordering = ('user',)