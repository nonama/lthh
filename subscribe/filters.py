# -*- coding: utf-8 -*-
from django import forms
from models import Subscribe
import django_filters

class SubFilter(django_filters.FilterSet):
    class Meta:
        model = Subscribe
        fields = {'user__profile__l_name': ['exact'],
                  'user__profile__f_name': ['exact'],
                  'user__profile__phone': ['exact'],
                  'area__area': ['exact'],
                 }