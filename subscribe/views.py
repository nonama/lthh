from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.http import JsonResponse
from filters import SubFilter

from models import Subscribe
from users.models import Area

def subscribe(request):
    if request.method == 'POST':
        aid = Area.objects.get(id=request.POST.get('area'))
        Subscribe.objects.update_or_create(user=request.user, area=aid)
    return JsonResponse({'status':'1'})

def unsubscribe(request, sid):
    regid = get_object_or_404(Subscribe, id=sid)
    area = regid.area
    if regid:
        Subscribe.objects.get(id=sid).delete()
    return render(request, 'unsubscribe.html', {'area' : area})

def subscribers(request):
    subscribers = SubFilter(request.GET, queryset = Subscribe.objects.all())
    return render(request, 'admin/subscribers.html', {'subscribers' : subscribers})

def delete(request, sid):
    regid = get_object_or_404(Subscribe, id=sid)
    area = regid.area
    if regid:
        Subscribe.objects.get(id=sid).delete()
    return HttpResponseRedirect('/masterzone/subscribers')