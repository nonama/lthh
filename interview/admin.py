# -*- coding: utf-8 -*-
from django.contrib import admin
from models import ProdCalendar, Office, OfficeCalendar, CitiesByArea

admin.site.register(ProdCalendar)
admin.site.register(Office)
admin.site.register(OfficeCalendar)

admin.site.register(CitiesByArea)