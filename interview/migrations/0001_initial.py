# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ProdCalendar',
            fields=[
                ('date', models.DateField(default=b'1900-01-01', unique=True, serialize=False, primary_key=True)),
                ('week_day', models.IntegerField()),
                ('day_type', models.CharField(max_length=1)),
            ],
            options={
                'ordering': ('date',),
            },
        ),
    ]
