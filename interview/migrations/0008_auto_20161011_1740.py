# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('interview', '0007_office_phone'),
    ]

    operations = [
        migrations.AlterField(
            model_name='office',
            name='city',
            field=models.CharField(max_length=90, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='email',
            field=models.CharField(max_length=90, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='phone',
            field=models.CharField(max_length=90, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='schedule',
            field=models.CharField(max_length=90, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='w1c',
            field=models.TimeField(default=b'18:00', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='w1o',
            field=models.TimeField(default=b'09:30', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='w2c',
            field=models.TimeField(default=b'18:00', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='w2o',
            field=models.TimeField(default=b'09:30', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='w3c',
            field=models.TimeField(default=b'18:00', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='w3o',
            field=models.TimeField(default=b'09:30', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='w4c',
            field=models.TimeField(default=b'18:00', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='w4o',
            field=models.TimeField(default=b'09:30', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='w5c',
            field=models.TimeField(default=b'18:00', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='w5o',
            field=models.TimeField(default=b'09:30', null=True, blank=True),
        ),
    ]
