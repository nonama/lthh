# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('interview', '0006_auto_20161011_1529'),
    ]

    operations = [
        migrations.AddField(
            model_name='office',
            name='phone',
            field=models.CharField(max_length=30, null=True, blank=True),
        ),
    ]
