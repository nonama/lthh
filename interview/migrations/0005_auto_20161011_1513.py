# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('interview', '0004_auto_20161011_1430'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='citiesbyarea',
            name='city',
        ),
        migrations.AddField(
            model_name='citiesbyarea',
            name='cid',
            field=models.ForeignKey(default=0, to='interview.Office'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='office',
            name='city',
            field=models.CharField(max_length=30, null=True),
        ),
    ]
