# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0015_auto_20160922_1054'),
        ('interview', '0003_officecalendar'),
    ]

    operations = [
        migrations.CreateModel(
            name='CitiesByArea',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('city', models.CharField(max_length=30, null=True)),
                ('aid', models.ForeignKey(to='users.Area')),
            ],
        ),
        migrations.RemoveField(
            model_name='officebyarea',
            name='aid',
        ),
        migrations.RemoveField(
            model_name='officebyarea',
            name='oid',
        ),
        migrations.RemoveField(
            model_name='office',
            name='name',
        ),
        migrations.AddField(
            model_name='office',
            name='email',
            field=models.CharField(max_length=30, null=True),
        ),
        migrations.AddField(
            model_name='office',
            name='schedule',
            field=models.CharField(max_length=30, null=True),
        ),
        migrations.DeleteModel(
            name='OfficeByArea',
        ),
        migrations.AddField(
            model_name='office',
            name='city',
            field=models.ForeignKey(default=0, to='interview.CitiesByArea'),
            preserve_default=False,
        ),
    ]
