# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('interview', '0002_office_officebyarea'),
    ]

    operations = [
        migrations.CreateModel(
            name='OfficeCalendar',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField()),
                ('wo', models.TimeField()),
                ('wc', models.TimeField(null=True)),
                ('oid', models.ForeignKey(to='interview.Office')),
            ],
        ),
    ]
