# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0007_auto_20160524_1022'),
        ('interview', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Office',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=60, blank=True)),
                ('address', models.TextField()),
                ('w1o', models.TimeField()),
                ('w1c', models.TimeField(null=True)),
                ('w2o', models.TimeField(null=True)),
                ('w2c', models.TimeField(null=True)),
                ('w3o', models.TimeField(null=True)),
                ('w3c', models.TimeField(null=True)),
                ('w4o', models.TimeField(null=True)),
                ('w4c', models.TimeField(null=True)),
                ('w5o', models.TimeField(null=True)),
                ('w5c', models.TimeField(null=True)),
                ('interval', models.IntegerField(null=True)),
                ('managers', models.IntegerField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='OfficeByArea',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('aid', models.ForeignKey(to='users.Area')),
                ('oid', models.ForeignKey(to='interview.Office')),
            ],
        ),
    ]
