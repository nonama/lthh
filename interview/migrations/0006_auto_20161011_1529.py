# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('interview', '0005_auto_20161011_1513'),
    ]

    operations = [
        migrations.AddField(
            model_name='office',
            name='map_id',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='address',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='city',
            field=models.CharField(max_length=30, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='email',
            field=models.CharField(max_length=30, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='interval',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='managers',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='schedule',
            field=models.CharField(max_length=30, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='w1c',
            field=models.TimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='w1o',
            field=models.TimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='w2c',
            field=models.TimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='w2o',
            field=models.TimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='w3c',
            field=models.TimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='w3o',
            field=models.TimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='w4c',
            field=models.TimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='w4o',
            field=models.TimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='w5c',
            field=models.TimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='w5o',
            field=models.TimeField(null=True, blank=True),
        ),
    ]
