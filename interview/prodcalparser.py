# coding: utf-8
import re
import urllib
from datetime import date

from bs4 import BeautifulSoup
from json import dumps
from pickle import dumps as pdumps


class ProdCalParser():
    def __init__(self, base_url, year, debug=False):
        self.base_url = base_url + str(year) + '/'
        self.year = int(year)
        self.days = {}
        self.debug = debug
        self.months_names = {
            u'Январь': 1,
            u'Февраль': 2,
            u'Март': 3,
            u'Апрель': 4,
            u'Май': 5,
            u'Июнь': 6,
            u'Июль': 7,
            u'Август': 8,
            u'Сентябрь': 9,
            u'Октябрь': 10,
            u'Ноябрь': 11,
            u'Декабрь': 12,
        }
        self._go()

    def _go(self):
        self.parse_calendar()

    def _get_soup(self, url):
        if self.debug:
            print 'fetch', url
        return BeautifulSoup(urllib.urlopen(url).read().decode('utf8'), "html.parser")

    def _to_date(self, year, month, day):
        return date(int(year), self.months_names.get(month), int(day))
    
    def _week_day(self, day):
        return self.week_days.get(day)

    def parse_calendar(self):
        url = self.base_url
        soup = self._get_soup(url)
        for month in soup.findAll('div', {'class': 'MonthsList_grid'}):
            month_text = month.find('div', {'class': 'h_text_align_center'}).get_text()
            for day in month.find('div', {'class': 'MonthsList_dates'}).findAll('div', {'class': 'MonthsList_date'}):
                cls = day.attrs.get("class")
                if ('h_color_gray' not in cls):
                    date_day = self._to_date(self.year, month_text, day.get_text())
                    if ('MonthsList_holiday' in day.attrs.get("class")):
                        self.days[date_day] = ('h', date_day.weekday()) 
                    elif ('MonthsList_preholiday' in day.attrs.get("class")):
                        self.days[date_day] = ('s', date_day.weekday()) 
                    else:
                        self.days[date_day] = ('w', date_day.weekday())

    #def serialize(self):
        #return dumps(dict([(x[0].isoformat(), x[1]) for x in self.days.iteritems()]))
    
    def serialize(self):
        return dict([(x[0].isoformat(), x[1]) for x in self.days.iteritems()])