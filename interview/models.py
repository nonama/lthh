# -*- coding: utf-8 -*-
from django.db import models
from users.models import Area
import time


class ProdCalendar(models.Model):
    date = models.DateField(unique=True, null=False, primary_key=True, default = '1900-01-01')
    week_day = models.IntegerField()
    day_type = models.CharField(max_length = 1)
    
    def __unicode__(self):
        return self.date.strftime('%Y-%m-%d')
    
    class Meta:
        ordering = ('date',)
        
        
class Office(models.Model):
    address = models.TextField(null=True, blank=True)
    w1o = models.TimeField(null=True, blank=True, default="09:30")
    w1c = models.TimeField(null=True, blank=True, default="18:00")
    w2o = models.TimeField(null=True, blank=True, default="09:30")
    w2c = models.TimeField(null=True, blank=True, default="18:00")
    w3o = models.TimeField(null=True, blank=True, default="09:30")
    w3c = models.TimeField(null=True, blank=True, default="18:00")
    w4o = models.TimeField(null=True, blank=True, default="09:30")
    w4c = models.TimeField(null=True, blank=True, default="18:00")
    w5o = models.TimeField(null=True, blank=True, default="09:30")
    w5c = models.TimeField(null=True, blank=True, default="18:00")
    interval = models.IntegerField(null=True, blank=True)
    managers = models.IntegerField(null=True, blank=True)
    schedule = models.CharField(null=True, blank=True, max_length = 90)
    email = models.CharField(null=True, blank=True, max_length = 90)
    city = models.CharField(null=True, blank=True, max_length = 90)
    phone = models.CharField(null=True, blank=True, max_length = 90)
    map_id = models.CharField(null=True, blank=True, max_length = 255)
    
    def __unicode__(self):
        return self.city

    
class CitiesByArea(models.Model):
    cid = models.ForeignKey(Office)
    aid = models.ForeignKey(Area)
    
    def __unicode__(self):
        return self.aid.area


class OfficeCalendar(models.Model):
    oid = models.ForeignKey(Office)
    date = models.DateField()
    wo = models.TimeField()
    wc = models.TimeField(null=True)

    def __unicode__(self):
        return self.date.strftime('%Y-%m-%d')