from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from models import ProdCalendar, Office, OfficeCalendar
from forms import OfficeForm
from users.models import Area
from prodcalparser import ProdCalParser
import datetime
from datetime import date
from django.core import serializers
import json as simplejson

BASE_URL = 'http://www.superjob.ru/proizvodstvennyj_kalendar/'

def design_demo(request):
    return render(request, 'index.html',)

def pc_update(request):
    u = request.user
    try:
        oid = CitiesByArea.objects.get(aid = u.profile.area.id)
    except:
        oid = 666
    curr_year = date.today().year
    cal = ProdCalendar.objects.all()
    out = []
    officies = Office.objects.all()
    if request.method == 'POST':
        if 'year' in request.POST:
            year = request.POST.get("year", "")
            s = ProdCalParser(BASE_URL, year)
            pre_calendar = s.serialize()
            ProdCalendar.objects.filter(date__year = year).delete()
            for key,value in pre_calendar.items():
                ProdCalendar.objects.create(date = key, week_day = value[1], day_type = value[0])
    for f in cal:
        out.append({'date' : str(f.date), 'wd' : f.week_day, 'dt' : f.day_type})
    data = simplejson.dumps(out)
    of = OfficeForm()
    if request.is_ajax():
        return HttpResponse(data, content_type='application/json')
    else:
        return render(request, 'pc_update.html', { 'cal': cal, 'curr_year' : curr_year, 'officies' : officies, 'oid' : oid, 'of' : of })


def office(request, oid):
    u = request.user
    curr_off = Office.objects.get(id = oid)
    off = serializers.serialize('json', Office.objects.filter(id = oid))
    data = simplejson.dumps(off)
    now = datetime.datetime.now()
    date = str(now.year) + "-" + str(now.month) + "-" + str(now.day)
    dates = ProdCalendar.objects.filter(date__gte = date)
    #dates = ProdCalendar.objects.all()
    if request.is_ajax():
        if request.method == 'GET':
            return HttpResponse(data, content_type='application/json')
        elif request.method == 'POST':
            schedule = Office(
                id=oid,
                name=request.POST.get('name'),
                address=request.POST.get('address'),
                w1o=request.POST.get('w1o'),
                w1c=request.POST.get('w1c'),
                w2o=request.POST.get('w2o'),
                w2c=request.POST.get('w2c'),
                w3o=request.POST.get('w3o'),
                w3c=request.POST.get('w3c'),
                w4o=request.POST.get('w4o'),
                w4c=request.POST.get('w4c'),
                w5o=request.POST.get('w5o'),
                w5c=request.POST.get('w5c'),
                interval=request.POST.get('interval'),
                managers=request.POST.get('managers'),
                )
            schedule.save()
            OfficeCalendar.objects.filter(oid = oid).filter(date__gte = date).delete()
            for d in dates:
                if d.day_type != 'h':
                    if d.week_day == 0:
                        wdo = request.POST.get('w1o')
                        wdc = request.POST.get('w1c')
                    elif d.week_day == 1:
                        wdo = request.POST.get('w2o')
                        wdc = request.POST.get('w2c')
                    elif d.week_day == 2:
                        wdo = request.POST.get('w3o')
                        wdc = request.POST.get('w3c')
                    elif d.week_day == 3:
                        wdo = request.POST.get('w4o')
                        wdc = request.POST.get('w4c')
                    elif d.week_day == 4:
                        wdo = request.POST.get('w5o')
                        wdc = request.POST.get('w5c')
                    OfficeCalendar.objects.create(oid = curr_off, date = d.date, wo = wdo, wc = wdc)
            return HttpResponse(data, content_type='application/json')

def office_create(request):
    dates = ProdCalendar.objects.all()
    if request.is_ajax():
        if request.method == 'POST':
            response_data = {}
            schedule = Office(
                name=request.POST.get('name'),
                address=request.POST.get('address'),
                w1o=request.POST.get('w1o'),
                w1c=request.POST.get('w1c'),
                w2o=request.POST.get('w2o'),
                w2c=request.POST.get('w2c'),
                w3o=request.POST.get('w3o'),
                w3c=request.POST.get('w3c'),
                w4o=request.POST.get('w4o'),
                w4c=request.POST.get('w4c'),
                w5o=request.POST.get('w5o'),
                w5c=request.POST.get('w5c'),
                interval=request.POST.get('interval'),
                managers=request.POST.get('managers'),
                )
            schedule.save()
            for d in dates:
                if d.day_type != 'h':
                    if d.week_day == 0:
                        wdo = request.POST.get('w1o')
                        wdc = request.POST.get('w1c')
                    elif d.week_day == 1:
                        wdo = request.POST.get('w2o')
                        wdc = request.POST.get('w2c')
                    elif d.week_day == 2:
                        wdo = request.POST.get('w3o')
                        wdc = request.POST.get('w3c')
                    elif d.week_day == 3:
                        wdo = request.POST.get('w4o')
                        wdc = request.POST.get('w4c')
                    elif d.week_day == 4:
                        wdo = request.POST.get('w5o')
                        wdc = request.POST.get('w5c')
                    OfficeCalendar.objects.create(oid = schedule, date = d.date, wo = wdo, wc = wdc)
            off = serializers.serialize('json', Office.objects.filter(id = schedule.id))
            data = simplejson.dumps(off)
            return HttpResponse(data, content_type='application/json')

def office_work_days(request, oid):
    owd = serializers.serialize('json', OfficeCalendar.objects.filter(oid=oid))
    if request.is_ajax():
        return HttpResponse(owd, content_type='application/json')


def office_work_day_update(request, oid):
    if request.is_ajax():
        if request.method == 'POST':
            oid = request.POST.get('oid')
            date = request.POST.get('date')
            wou = request.POST.get('wo')
            wcu = request.POST.get('wc')
            odu = OfficeCalendar.objects.filter(oid=oid).filter(date=date)
            odu.update(wo = wou, wc = wcu)
            owd = serializers.serialize('json', OfficeCalendar.objects.filter(oid=oid))
            return HttpResponse(owd, content_type='application/json')