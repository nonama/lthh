# -*- coding: utf-8 -*-
from django.contrib import admin
from models import Cities, AreaByIP

admin.site.register(Cities)
admin.site.register(AreaByIP)

