# -*- coding: utf-8 -*-
from django.db import models


class Cities(models.Model):
    aid = models.CharField(primary_key = True, unique=True, max_length = 30)
    rid = models.CharField(max_length = 30)
    
    def __unicode__(self):
        return self.aid


class AreaByIP(models.Model):
    ip = models.CharField(max_length = 30)
    aid = models.ForeignKey(Cities)

    def __unicode__(self):
        return self.ip