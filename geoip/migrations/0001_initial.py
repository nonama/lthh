# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AreaByIP',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ip', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Cities',
            fields=[
                ('aid', models.CharField(max_length=30, unique=True, serialize=False, primary_key=True)),
                ('rid', models.CharField(max_length=30)),
            ],
        ),
        migrations.AddField(
            model_name='areabyip',
            name='aid',
            field=models.ForeignKey(to='geoip.Cities'),
        ),
    ]
