# -*- coding: utf-8 -*-
from django.contrib import admin
from employee.models import Employee, EmployeeRelation, Section, SectionContent, BankCardStatus, Documents

admin.site.register(Section)
admin.site.register(SectionContent)
admin.site.register(BankCardStatus)
admin.site.register(Documents)
admin.site.register(Employee)
admin.site.register(EmployeeRelation)