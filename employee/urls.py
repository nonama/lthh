from django.conf.urls import url

from django.conf.urls.static import static
from django.conf import settings
import views


urlpatterns = [
  
    url(r'^structure$', views.structure, name='structure'),
    url(r'library$', views.library, name='library'),
    url(r'bankcard$', views.bankcard, name='bankcard'),
    url(r'kpi$', views.kpi, name='kpi'),
    url(r'education$', views.education, name='education'),
    
]