from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from models import SectionContent, Section, BankCardStatus, Documents, EmployeeRelation, Employee
from users.models import Profile
from forms import DocumentForm

@login_required
def structure(request):
    uid = request.user
    bid = uid.profile.badge_id
    if bid:
        try:
            second_level = EmployeeRelation.objects.get(child = uid.profile.badge_id).parent
            first_level = EmployeeRelation.objects.get(child = second_level)
            own = Employee.objects.get(badge_id = uid.profile.badge_id)
        
            director = Employee.objects.get(badge_id = first_level)
            sub_director = Employee.objects.get(badge_id = second_level)
        except:
            director = ''
            try: 
                second_level = EmployeeRelation.objects.get(child = uid.profile.badge_id).parent
                sub_director = Employee.objects.get(badge_id = second_level)
                own = Employee.objects.get(badge_id = uid.profile.badge_id)
            except:
                sub_director = ''
                own = Profile.objects.get(badge_id = uid.profile.badge_id)
    else:
        director = ''
        sub_director = ''
        own = Profile.objects.get(user_id = uid.id)
    return render(request, 'structure.html', {'fl' : director, 'sl' : sub_director, 'own' : own , 'bid' : bid})

@login_required
def library(request):
    doc_general = Documents.objects.filter(Q(badge_id__isnull=True) | Q(badge_id__exact=''))
    doc_personal = Documents.objects.filter(badge_id = request.user.profile.badge_id)
    return render(request, 'library.html', {'general' : doc_general, 'personal' : doc_personal})


@login_required
def admin_library(request):
    doc_general = Documents.objects.filter(Q(badge_id__isnull=True) | Q(badge_id__exact=''))
    doc_personal = Documents.objects.filter(badge_id__isnull=False).exclude(badge_id__exact='')
    if request.method == 'POST':
        doc_form = DocumentForm(request.POST, request.FILES)
        if doc_form.is_valid():
            doc_form.save()
    else:
        doc_form = DocumentForm()
    return render(request, 'admin/documents.html', {'general' : doc_general, 'personal' : doc_personal, 'doc_form' : doc_form})

def admin_library_delete(request, did):
    doc = Documents.objects.get(id=did).delete()
    return HttpResponseRedirect('/masterzone/documents')

@login_required
def bankcard(request):
    try:
        message = SectionContent.objects.filter(section__name = "MyBankCard").filter(badge_id = request.user.profile.badge_id)[0]
    except:
        message = ''
    return render(request, 'bank_card.html', {'message' : message})

@login_required
def kpi(request):
    kpi_general = SectionContent.objects.filter(section__name = "MyKPI").filter(Q(badge_id__isnull=True) | Q(badge_id__exact=''))
    kpi_personal = SectionContent.objects.filter(section__name = "MyKPI").filter(badge_id = request.user.profile.badge_id)
    return render(request, 'kpi.html', {'general' : kpi_general, 'personal' : kpi_personal })

@login_required
def education(request):
    edu_general = SectionContent.objects.filter(section__name = "MyEducation").filter(Q(badge_id__isnull=True) | Q(badge_id__exact=''))
    edu_personal = SectionContent.objects.filter(section__name = "MyEducation").filter(badge_id = request.user.profile.badge_id)
    return render(request, 'education.html', {'general' : edu_general, 'personal' : edu_personal })