# -*- coding: utf-8 -*-
from django.db import models
from datetime import datetime

from django.db.models.signals import post_delete
from django.dispatch import receiver


class Employee(models.Model):
    f_name = models.CharField(blank=True, max_length=64)
    m_name = models.CharField(blank=True, max_length=64)
    l_name = models.CharField(blank=True, max_length=64)
    email = models.CharField(blank=True, max_length=64)
    phone = models.CharField(blank=True, max_length=64)
    position = models.CharField(blank=True, max_length=127)
    active = models.BooleanField(default=True)
    badge_id = models.CharField(unique=True, max_length=10)
    avatar = models.CharField(blank=True, null=True, max_length=127)
    
    def __unicode__(self):
        return self.l_name


class EmployeeRelation(models.Model):
    child = models.ForeignKey(Employee, to_field='badge_id')
    parent = models.CharField(blank=True, null=True, max_length=10)
    
    def __unicode__(self):
        return self.parent

class Documents(models.Model):
    doc_name = models.CharField(null=True, blank=True, max_length=256)
    doc_type = models.CharField(null=True, blank=True, max_length=3)
    doc_date = models.DateField(blank=True, auto_now_add=True)
    badge_id = models.CharField(null=True, blank=True, max_length=64)
    doc_path = models.FileField(upload_to='documents/')
    
    def __unicode__(self):
        return self.doc_name
    
@receiver(post_delete, sender=Documents)
def document_post_delete_handler(sender, **kwargs):
    document = kwargs['instance']
    storage, path = document.doc_path.storage, document.doc_path.path
    storage.delete(path)
    
class BankCardStatus(models.Model):
    badge_id = models.CharField(null=True, blank=True, max_length=64)
    message = models.CharField(null=True, blank=True, max_length=255)
    
    def __unicode__(self):
        return self.badge_id
    
    
class Section(models.Model):
    name = models.CharField(null=True, blank=True, max_length=64)
    
    def __unicode__(self):
        return self.name
    
    
class SectionContent(models.Model):
    section = models.ForeignKey(Section)
    badge_id = models.CharField(null=True, blank=True, max_length=64)
    content = models.TextField()
    status = models.BooleanField(default=True)
    
    def __unicode__(self):
        return self.content

    
