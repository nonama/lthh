# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0003_auto_20160901_1553'),
    ]

    operations = [
        migrations.CreateModel(
            name='BankCardStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('badge_id', models.CharField(max_length=64, null=True, blank=True)),
                ('message', models.CharField(max_length=255, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Documents',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('badge_id', models.CharField(max_length=64, null=True, blank=True)),
                ('doc_name', models.CharField(max_length=64, null=True, blank=True)),
                ('doc_path', models.CharField(max_length=255, null=True, blank=True)),
                ('doc_type', models.CharField(max_length=3, null=True, blank=True)),
                ('doc_date', models.CharField(max_length=10, null=True, blank=True)),
                ('status', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='Section',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='SectionContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('badge_id', models.CharField(max_length=64, null=True, blank=True)),
                ('content', models.TextField()),
                ('status', models.BooleanField(default=True)),
                ('section', models.ForeignKey(to='employee.Section')),
            ],
        ),
    ]
