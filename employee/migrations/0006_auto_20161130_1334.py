# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0005_auto_20161130_1331'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documents',
            name='doc_date',
            field=models.DateField(auto_now_add=True),
        ),
    ]
