# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0002_auto_20160831_1542'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employee',
            name='avatar',
            field=models.CharField(max_length=127, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='employee',
            name='badge_id',
            field=models.CharField(unique=True, max_length=10),
        ),
        migrations.AlterField(
            model_name='employee',
            name='email',
            field=models.CharField(max_length=64, blank=True),
        ),
        migrations.AlterField(
            model_name='employee',
            name='f_name',
            field=models.CharField(max_length=64, blank=True),
        ),
        migrations.AlterField(
            model_name='employee',
            name='l_name',
            field=models.CharField(max_length=64, blank=True),
        ),
        migrations.AlterField(
            model_name='employee',
            name='m_name',
            field=models.CharField(max_length=64, blank=True),
        ),
        migrations.AlterField(
            model_name='employee',
            name='phone',
            field=models.CharField(max_length=64, blank=True),
        ),
        migrations.AlterField(
            model_name='employee',
            name='position',
            field=models.CharField(max_length=127, blank=True),
        ),
        migrations.AlterField(
            model_name='employeerelation',
            name='parent',
            field=models.CharField(max_length=10, null=True, blank=True),
        ),
    ]
