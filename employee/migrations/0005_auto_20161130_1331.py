# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0004_bankcardstatus_documents_section_sectioncontent'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='documents',
            name='status',
        ),
        migrations.AlterField(
            model_name='documents',
            name='doc_path',
            field=models.FileField(upload_to=b'documents/'),
        ),
    ]
