# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='employee',
            name='email',
            field=models.CharField(max_length=30, blank=True),
        ),
        migrations.AddField(
            model_name='employee',
            name='phone',
            field=models.CharField(max_length=30, blank=True),
        ),
        migrations.AlterField(
            model_name='employee',
            name='badge_id',
            field=models.CharField(default=0, unique=True, max_length=30),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='employeerelation',
            name='child',
            field=models.ForeignKey(to='employee.Employee', default=0, to_field=b'badge_id'),
            preserve_default=False,
        ),
    ]
