# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('f_name', models.CharField(max_length=30, blank=True)),
                ('m_name', models.CharField(max_length=30, blank=True)),
                ('l_name', models.CharField(max_length=30, blank=True)),
                ('position', models.CharField(max_length=30, blank=True)),
                ('active', models.BooleanField(default=True)),
                ('badge_id', models.CharField(max_length=25, null=True, blank=True)),
                ('avatar', models.CharField(max_length=25, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='EmployeeRelation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('parent', models.CharField(max_length=25, null=True, blank=True)),
                ('child', models.CharField(max_length=25, null=True, blank=True)),
            ],
        ),
    ]
