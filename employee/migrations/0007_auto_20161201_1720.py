# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0006_auto_20161130_1334'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documents',
            name='doc_name',
            field=models.CharField(max_length=256, null=True, blank=True),
        ),
    ]
