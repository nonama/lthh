/*global jQuery*/
/*global $*/
function getCookie(name) {
          var cookieValue = null;
          if (document.cookie && document.cookie != '') {
                var cookies = document.cookie.split(';');
          for (var i = 0; i < cookies.length; i++) {
               var cookie = jQuery.trim(cookies[i]);
          // Does this cookie string begin with the name we want?
          if (cookie.substring(0, name.length + 1) == (name + '=')) {
            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
              break;
             }
          }
      }
 return cookieValue;
}

var currentTime = new Date()
var year = currentTime.getFullYear()
var next_year = currentTime.getFullYear() + 1
var start = new Date("1/1/" + year);
var end = new Date("12/31/" + next_year);
var holidays = new Array
var hol_raw = new Array
var data = {}
var owd_raw = new Array
var owd = new Array

function getHolidays(){
    $.ajax({
        url: 'pc_update',
        type: "GET",
        dataType: 'json',
        data: data,
        async:false,
        success: function (data) {
          for (var i in data) {
              if (data[i]['dt'] == 'h') {
                  cd = data[i]['date'].split("-")
                  holidays.push(new Date(cd).getTime())
                  hol_raw.push(new Date(cd))
              };
          };
        },
    });
};

function getOfficeWorkDays(oid){
    owd_raw = []
    owd = []
    $.ajax({
        url: 'api/owd/' + oid,
        type: "GET",
        dataType: 'json',
        data: data,
        async:false,
        success: function (data) {
          for (var i in data) {
              cd = data[i]['fields']['date'].split("-")
              owd_raw.push(new Date(cd).getTime())
              owd.push(data[i]['fields'])
          };
        },
    });
};

function getOffice(oid){
    if (oid != 666) {
            $.ajax({
                url: 'api/office/' + oid,
                type: "GET",
                dataType: 'json',
                data: data,
                async:false,
                success: function (data) {
                    var arr = JSON.parse(data)[0]['fields']
                    $('#id_name').val(arr['name'])
                    $('#id_address').val(arr['address'])
                    $('#id_w1o').val(arr['w1o'].slice(0,-3))
                    $('#id_w1c').val(arr['w1c'].slice(0,-3))
                    $('#id_w2o').val(arr['w2o'].slice(0,-3))
                    $('#id_w2c').val(arr['w2c'].slice(0,-3))
                    $('#id_w3o').val(arr['w3o'].slice(0,-3))
                    $('#id_w3c').val(arr['w3c'].slice(0,-3))
                    $('#id_w4o').val(arr['w4o'].slice(0,-3))
                    $('#id_w4c').val(arr['w4c'].slice(0,-3))
                    $('#id_w5o').val(arr['w5o'].slice(0,-3))
                    $('#id_w5c').val(arr['w5c'].slice(0,-3))
                    $('#id_interval').val(arr['interval'])
                    $('#id_managers').val(arr['managers'])
                }
            });
    } else {
                    $('#id_name').val('')
                    $('#id_address').val('')
                    $('#id_w1o').val('')
                    $('#id_w1c').val('')
                    $('#id_w2o').val('')
                    $('#id_w2c').val('')
                    $('#id_w3o').val('')
                    $('#id_w3c').val('')
                    $('#id_w4o').val('')
                    $('#id_w4c').val('')
                    $('#id_w5o').val('')
                    $('#id_w5c').val('')
                    $('#id_interval').val('')
                    $('#id_managers').val('')
                    $('#submit_year_update_form').html('Создать')
    }
}

function submitOffice(oid){
    $('#year_update_form').addClass('loading');
    if (oid != 666) {
            $.ajax({
                url: 'api/office/' + oid,
                type: "POST",
                dataType: 'json',
                headers: {'X-CSRFToken': getCookie('csrftoken')},
                data: {
                    name : $('#id_name').val(),
                    address : $('#id_address').val(),
                    w1o : $('#id_w1o').val(),
                    w1c : $('#id_w1c').val(),
                    w2o : $('#id_w2o').val(),
                    w2c : $('#id_w2c').val(),
                    w3o : $('#id_w3o').val(),
                    w3c : $('#id_w3c').val(),
                    w4o : $('#id_w4o').val(),
                    w4c : $('#id_w4c').val(),
                    w5o : $('#id_w5o').val(),
                    w5c : $('#id_w5c').val(),
                    interval : $('#id_interval').val(),
                    managers : $('#id_managers').val(),
                },
                async:false,
                success: function (data) {
                    console.log("success");
                    getOfficeWorkDays(oid);
                },
                error : function(xhr,errmsg,err) {
                    console.log(errmsg);
                }
            });
        } else {
            $.ajax({
                url: 'api/office',
                type: "POST",
                dataType: 'json',
                headers: {'X-CSRFToken': getCookie('csrftoken')},
                data: {
                    name : $('#id_name').val(),
                    address : $('#id_address').val(),
                    w1o : $('#id_w1o').val(),
                    w1c : $('#id_w1c').val(),
                    w2o : $('#id_w2o').val(),
                    w2c : $('#id_w2c').val(),
                    w3o : $('#id_w3o').val(),
                    w3c : $('#id_w3c').val(),
                    w4o : $('#id_w4o').val(),
                    w4c : $('#id_w4c').val(),
                    w5o : $('#id_w5o').val(),
                    w5c : $('#id_w5c').val(),
                    interval : $('#id_interval').val(),
                    managers : $('#id_managers').val(),
                },
                async:false,
                success: function (data) {
                    console.log("success");
                    var arr = JSON.parse(data)[0]['fields']
                    $('#id_name').val(arr['name'])
                    $('#id_address').val(arr['address'])
                    $('#id_w1o').val(arr['w1o'])
                    $('#id_w1c').val(arr['w1c'])
                    $('#id_w2o').val(arr['w2o'])
                    $('#id_w2c').val(arr['w2c'])
                    $('#id_w3o').val(arr['w3o'])
                    $('#id_w3c').val(arr['w3c'])
                    $('#id_w4o').val(arr['w4o'])
                    $('#id_w4c').val(arr['w4c'])
                    $('#id_w5o').val(arr['w5o'])
                    $('#id_w5c').val(arr['w5c'])
                    $('#id_interval').val(arr['interval'])
                    $('#id_managers').val(arr['managers'])
                    $('#year_update_form').removeClass('loading');
                },
                error : function(xhr,errmsg,err) {
                    console.log(errmsg);
                }
            });
        }
}

function workDayUpd(oid){
    if (oid != 666) {
            $.ajax({
                url: 'api/office/wdu/' + oid,
                type: "POST",
                dataType: 'json',
                headers: {'X-CSRFToken': getCookie('csrftoken')},
                data: {
                    oid : $( "#os" ).val(),
                    date : $('#curr_date').val(),
                    wo : $('#wou').val(),
                    wc : $('#wcu').val(),
                },
                async:false,
                success: function (data) {
                    console.log("success");
                    getOfficeWorkDays(oid);
                    location.reload();
                },
                error : function(xhr,errmsg,err) {
                    console.log(errmsg);
                }
            });
        }
}

$(document).ready(function(){
    var oid = $( "#os" ).val();
    getHolidays();
    getOffice(oid);
    getOfficeWorkDays(oid);
    
    $('#os').dropdown({
        onChange:function(value,text){
            getOffice(value);
            getOfficeWorkDays(value);
            $('#calendar').calendar({
                minDate: start,
                maxDate: end,
                language : 'ru',
                startYear: year,
                disabledDays: hol_raw,
                customDayRenderer: function(element, date) {
                    if($.inArray(date.getTime(), holidays) > -1) {
                        $(element).css('background-color', '#ff6f69');
                        $(element).css('color', 'white');
                        $(element).css('border-radius', '50%');
                    } else if ($.inArray(date.getTime(), owd_raw) > -1) {
                        for (var i in owd) {
                            if ( new Date(owd[i]['date'].split("-")).getTime() == date.getTime()){
                                $(element).popup({
                                    title   : 'Режим работы',
                                    content : 'С ' + owd[i]['wo'].slice(0,-3) + ' до ' + owd[i]['wc'].slice(0,-3),
                                    variation : 'basic'
                                });
                            }
                        };
                    };
                },
                clickDay: function(e) {
                    $('#day_change').modal('show');
                    $('#date_change').html((new Date(e.date)).toString('dd.MM.yyyy'));
                    var dt = (new Date(e.date)).toString('yyyy-MM-dd')
                    for (var i in owd) {
                        if (owd[i]['date'] == dt){
                            var wc = owd[i]['wc'].slice(0,-3)
                            var wo = owd[i]['wo'].slice(0,-3)
                        }
                    };
                    $('#wou').val(wo);
                    $('#wcu').val(wc);
                    $('#curr_date').val(dt)
                }
            });
        }
    });

    $('#calendar').calendar({
      minDate: start,
      maxDate: end,
      language : 'ru',
      startYear: year,
      disabledDays: hol_raw,
      customDayRenderer: function(element, date) {
          if($.inArray(date.getTime(), holidays) > -1) {
               $(element).css('background-color', '#ff6f69');
               $(element).css('color', 'white');
               $(element).css('border-radius', '50%');
          } else if ($.inArray(date.getTime(), owd_raw) > -1) {
              for (var i in owd) {
                  if ( new Date(owd[i]['date'].split("-")).getTime() == date.getTime()){
                      $(element).popup({
                        title   : 'Режим работы',
                        content : 'С ' + owd[i]['wo'].slice(0,-3) + ' до ' + owd[i]['wc'].slice(0,-3),
                        variation : 'basic'
                      });
                  };
              };
          };
      },
      clickDay: function(e) {
          $('#day_change').modal('show');
          $('#date_change').html((new Date(e.date)).toString('dd.MM.yyyy'));
          var dt = (new Date(e.date)).toString('yyyy-MM-dd')
          for (var i in owd) {
              if (owd[i]['date'] == dt){
                 var wc = owd[i]['wc'].slice(0,-3)
                 var wo = owd[i]['wo'].slice(0,-3)
              }
          };
          $('#wou').val(wo);
          $('#wcu').val(wc);
          $('#curr_date').val(dt)
      }
    });
    
    $('#submit_year_update_form').on('click', function(e){
        var oid = $( "#os" ).val();
        e.preventDefault();
        console.log("form submitted!");
        submitOffice(oid);
        location.reload();
    });

    $('#wdu_submit').on('click', function(e){
        var oid = $( "#os" ).val();
        e.preventDefault();
        console.log("form submitted!");
        workDayUpd(oid);
        
    });
});