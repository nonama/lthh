function isValidDate(year, month, day) {
    var d = new Date(year, month, day);
    if (d.getFullYear() == year && d.getMonth() == month && d.getDate() == day) {
        return true;
    }
    return false;
}

$(document).ready(function() {
  function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
  }
  
  if($( "#c1" ).is(':checked')){
            $('.temp_reg_block').removeClass('hidden');
        }
    $('input[name="temp_reg"]:radio').change(function() {       
        if($( "input:radio[name=temp_reg]:checked" ).val() === "True"){
            $('.temp_reg_block').removeClass('hidden');
        } else {
            $('.temp_reg_block').addClass('hidden');
        }
    });
    
    if($( "#c5" ).is(':checked')){
            $('.child_block').removeClass('hidden');
        }
    $('input[name="children"]:radio').change(function() {       
        if($( "input:radio[name=children]:checked" ).val() === "True"){
            $('.child_block').removeClass('hidden');
        } else {
            $('.child_block').addClass('hidden');
        }
    });
    
    if($( "#c11" ).is(':checked')){
            $('.med_block').removeClass('hidden');
        }
    $('input[name="med_book"]:radio').change(function() {       
        if($( "input:radio[name=med_book]:checked" ).val() === "True"){
            $('.med_block').removeClass('hidden');
        } else {
            $('.med_block').addClass('hidden');
        }
    });
    
    if($( "#c13" ).is(':checked')){
            $('.inv_block').removeClass('hidden');
        }
    $('input[name="disability"]:radio').change(function() {       
        if($( "input:radio[name=disability]:checked" ).val() === "True"){
            $('.inv_block').removeClass('hidden');
        } else {
            $('.inv_block').addClass('hidden');
        }
    });
  
  var out, data = [];
  $('#id_bd_month, #id_doc_type, #id_citizenship, #id_doc_type, #id_known_from_list, #id_area').dropdown();

  $('#id_issued_date, #id_temp_reg_date, #id_temp_reg_last_date, #id_mb_date, #id_med_exam_date').mask("99.99.9999");
  $('#req_modal').on('hide.bs.modal', function (e) {
    $('#profile_form').form('reset');
    $('#profile_form_errors').hide();
  });
  
  var curr_year = new Date().getFullYear();
  $('#id_phone').mask("+7 (999) 999-99-99");
  $('#success_info').hide();
  var check = $('#filled_check').val();
  $('#for_filled_check_jobs, #for_filled_check_job, #for_filled_check_reply').click(function(e){
	  if(check === 'false'){
        console.log(check)
  			e.preventDefault();
        $('#req_modal').modal('show');
    } else {
      return true;
    }
	});
  $('#profile_form').form({
    transition: 'fade',
    on: 'blur',
    fields: {
      phone: {
        identifier: 'phone',
        rules: [
          {
            type   : 'empty',
            prompt : 'Пожалуйста, введите телефон формата +7 (ххх) ххх-хх-хх.',
            
          }
        ]
      },
      f_name: {
        identifier: 'f_name',
        rules: [
          {
            type   : 'empty',
            prompt : 'Пожалуйста, введите Имя.',
            
          }
        ]
      },
      l_name: {
        identifier: 'l_name',
        rules: [
          {
            type   : 'empty',
            prompt : 'Пожалуйста, введите Фамилию.',
            
          }
        ]
      },
      bd_day: {
        identifier: 'bd_day',
        rules: [
          {
            type   : 'integer[1..31]',
            prompt : 'Пожалуйста, введите корректный день.',
            
          }
        ]
      },
      bd_month: {
        identifier: 'bd_month',
        rules: [
          {
            type   : 'empty',
            prompt : 'Пожалуйста, выберите месяц.',
            
          }
        ]
      },
      bd_year: {
        identifier: 'bd_year',
        rules: [
          {
            type   : 'integer[1920..'+ curr_year + ']',
            prompt : 'Пожалуйста, введите корректный год.',
            
          }
        ]
      },
      area: {
        identifier: 'area',
        rules: [
          {
            type   : 'empty',
            prompt : 'Пожалуйста, выберите регион.',
            
          }
        ]
      },
      citizenship: {
        identifier: 'citizenship',
        rules: [
          {
            type   : 'empty',
            prompt : 'Пожалуйста, выберите гражданство.',
            
          }
        ]
      },
    },
    onSuccess : function(e){
        var year = $('#id_bd_year').val();
        var month = $('#id_bd_month').val();
        var day = $('#id_bd_day').val();
        var date = year + '-' + month + '-' + day;
        var check_date = moment(date);

        if (check_date.isValid()) {
          var data = $('#profile_form').serialize();
			    $.ajax({
            type: 'POST',
            url: '/profile',
            data: data,
				    headers: {
    			    'X-CSRFToken': getCookie('csrftoken')
  			    },
            success: function (data) {
         	    $('#profile_form').hide();
              $('#modalHeader').hide();
              $('#success_info').show();
              check = 'true';
            },
            error: function(data) {
              console.log("Something went wrong!");
				    }
        	});  
        } else {
            $('#date_check').show();
            $('#date_check').delay(3000).fadeOut(500);
            return false;
        }
        return false;
    } 
  });
});