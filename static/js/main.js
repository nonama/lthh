$(document).ready(function() {
$('.vac-slider').owlCarousel({
        loop:true,
        center:false,
        margin:10,
        nav:true,
        autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            800:{
                items:4
            },
            1000:{
                items:6
            }
        }
    })

$('.part-slider').owlCarousel({
        loop:true,
        center:true,
        margin:20,
        nav:false,
        autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
        responsive:{
            0:{
                items:2
            },
            600:{
                items:3
            },
            800:{
                items:4
            },
            1000:{
                items:5
            }
        }
    })

	$('#signup').on('click', function(){
			$('#fade-screen').show();
			$('#reg_form').show();
			$('body').css('overflow', 'hidden');
	});
	$('#reg_form_close').on('click', function(){
			$('#fade-screen').hide();
			$('#reg_form').hide();
			$('body').css('overflow', 'auto');
	});
	$('#signup_but, #signup_but2').click(function(){
			$('#fade-screen').show();
			$('#reg_form').show();
			$('body').css('overflow', 'hidden');
	});
	$('#login').on('click', function(){
			$('#fade-screen').show();
			$('#login_form').show();
			$('body').css('overflow', 'hidden');
	});
	$('#login_form_close').on('click', function(){
			$('#fade-screen').hide();
			$('#login_form').hide();
			$('body').css('overflow', 'auto');
	});
	
	/*----------------------------------------------------CALLBACK PROCESS----------------------------------------------*/
	$('#callback').on('click', function(){
			$('#fade-screen').show();
			$('#callback_form').show();
			$('body').css('overflow', 'hidden');
			$('#id_regions').dropdown();
	});
	$('#call_form_close').on('click', function(){
			$('#fade-screen').hide();
			$('#callback_form').hide();
			$('body').css('overflow', 'auto');
	});
	$('#call_succ_form_close').on('click', function(){
			$('#fade-screen').hide();
			$('#callback_success_form').hide();
			$('body').css('overflow', 'auto');
	});
	
	$('#id_phone_call, #id_phone').mask("?+7 (999) 999-99-99");
	
	var frm = $('#call_form');
	frm.form({
    inline:true,
    transition: 'fade',
    on: 'submit',
    fields: {
      phone: {
        identifier: 'phone',
        rules: [
          {
            type   : 'empty',
            prompt : 'Пожалуйста, введите телефон формата +7 (ххх) ххх-хх-хх.',
            
          },
					{
            type   : 'exactLength[18]',
            prompt : 'Пожалуйста, введите телефон формата +7 (ххх) ххх-хх-хх.',
            
          },
        ]
      },
      area: {
        identifier: 'area',
        rules: [
          {
            type   : 'empty',
            prompt : 'Пожалуйста, введите корректный день.',
            
          }
        ]
      },
    },
});
  frm.submit(function () {
				var phn = $('#id_phone_call').val();
				if (phn.length == 18) {
					$.ajax({
            			type: frm.attr('method'),
            			url: '/callback',
            			data: frm.serialize(),
            			success: function (data) {
                				$("#callback_form").hide();
								$("#callback_success_form").show();
            			},
            			error: function(data) {
                				console.log("Something went wrong!");
						}
        			});
					return false;
				} else {
					return false;
				}
        
    			});
	var sign_form = $('#signup_form');
	sign_form.form({
    inline:true,
    transition: 'fade',
    on: 'submit',
    fields: {
	  	email: {
        identifier: 'email',
        rules: [
          {
            type   : 'email',
            prompt : 'Введите корректный адрес',
            
          },
					{
            type   : 'empty',
            prompt : 'Обязательное поле!',
            
          },
        ]
      },
      phone: {
        identifier: 'phone',
        rules: [
          {
            type   : 'empty',
            prompt : 'Пожалуйста, введите телефон формата +7 (ххх) ххх-хх-хх.',
            
          },
					{
            type   : 'exactLength[18]',
            prompt : 'Пожалуйста, введите телефон формата +7 (ххх) ххх-хх-хх.',
            
          },
        ]
      },
			password1: {
        identifier: 'password1',
        rules: [
          {
            type   : 'empty',
            prompt : 'Обязательное поле!',
            
          },
					{
            type   : 'minLength[6]',
            prompt : 'Пароль должен быть не менее 6 символов.',
            
          },
        ]
      },
			password2: {
        identifier: 'password2',
        rules: [
          {
            type   : 'empty',
            prompt : 'Обязательное поле!',
            
          },
					{
            type   : 'minLength[6]',
            prompt : 'Пароль должен быть не менее 6 символов.',
            
          },
			{
            type   : 'match[password1]',
            prompt : 'Пароли не совпадают.'
          }
        ]
      },
			privacy: {
        identifier: 'privacy',
        rules: [
          {
            type   : 'checked',
            prompt : 'Обязательное поле!',
            
          },
        ]
      },
		},
		onSuccess(submit, email, phone, password1, password2, privacy){
			console.log(event);
    	$('#regSubmitButt').button('loading');
		}
});
	sign_form.submit(function(){
		$("#id_username").val($("#id_email").val());
		var phone = $('#id_phone').val();
		var privacy = $('#id_privacy')
			if (phone.length == 18) {
				if (privacy.is(":checked")){
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
	});
	
	var user_aid = $("#user_aid").val();
	var area_check = $('#id_area').val();
	//if (area_check.length === 0) { 
		//$('#id_area').val(user_aid);
	//}
});



