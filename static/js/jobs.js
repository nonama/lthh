$(document).ready(function() {
	function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
	
    $('#id_position').dropdown();
	$('#id_area').dropdown();
	$('.cancel_modal').click(function(){
		var vid = $(this).data('id');
		
		var form = $(this).parent().find('reply_form');
		var data = {vacancy: vid}
		$('#delete_reply').click(function(){
			$.ajax({
            	type: 'POST',
            	url: '/jobs',
            	data: data,
				headers: {
    			'X-CSRFToken': getCookie('csrftoken')
  			},
            	success: function (data) {
         			location.href = '/replies'
            	},
            	error: function(data) {
                	console.log("Something went wrong!");
				}
        	});
			
			
		});
	});
  });