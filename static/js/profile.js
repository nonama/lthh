/*global $ */
$(document).ready(function(){
    if($( "#c1" ).is(':checked')){
            $('.temp_reg_block').removeClass('hidden');
        }
    $('input[name="temp_reg"]:radio').change(function() {       
        if($( "input:radio[name=temp_reg]:checked" ).val() === "True"){
            $('.temp_reg_block').removeClass('hidden');
        } else {
            $('.temp_reg_block').addClass('hidden');
        }
    });
    
    if($( "#c5" ).is(':checked')){
            $('.child_block').removeClass('hidden');
        }
    $('input[name="children"]:radio').change(function() {       
        if($( "input:radio[name=children]:checked" ).val() === "True"){
            $('.child_block').removeClass('hidden');
        } else {
            $('.child_block').addClass('hidden');
        }
    });
    
    if($( "#c11" ).is(':checked')){
            $('.med_block').removeClass('hidden');
        }
    $('input[name="med_book"]:radio').change(function() {       
        if($( "input:radio[name=med_book]:checked" ).val() === "True"){
            $('.med_block').removeClass('hidden');
        } else {
            $('.med_block').addClass('hidden');
        }
    });
    
    if($( "#c13" ).is(':checked')){
            $('.inv_block').removeClass('hidden');
        }
    $('input[name="disability"]:radio').change(function() {       
        if($( "input:radio[name=disability]:checked" ).val() === "True"){
            $('.inv_block').removeClass('hidden');
        } else {
            $('.inv_block').addClass('hidden');
        }
    });
    
    
/*    var def_av = $('#curr_av').attr('src');

    $('#image-cropper').cropit({ imageState: { src: def_av},
                                 onFileChange: function(){
                                   $('.cropit-image-zoom-input').css('visibility', 'visible');
                                 }
                               });
$('#ava_load').click(function(e) {
  e.preventDefault();
  $('.cropit-image-input').click();
});*/

var out, data = [];
$('#id_bd_month, #id_doc_type, #id_citizenship, #id_doc_type, #id_known_from_list, #id_area').dropdown();

$('#id_issued_date, #id_temp_reg_date, #id_temp_reg_last_date, #id_mb_date, #id_med_exam_date').mask("99.99.9999");
$('#req_modal').on('hide.bs.modal', function (e) {
  $('#profile_form').form('reset');
  $('#profile_form_errors').hide();
});
/*var ureg = $('#reg_check').val();
var umet = $('#met_check').val();
if (umet){
    $('.metro_block').show();
} else {
    $('.metro_block').hide();
}
$.ajax({
        url: 'api/profile/metros/' + (+ureg),
        type: "GET",
        dataType: 'json',
        data: out,
        async:false,
        success: function (out) {
            for (var d in out){
              data.push(out[d]);
            }
          }
    });
for (var i in data) {
    if (data[i].id === +umet){
        $('#id_metro').append($('<option selected></option>').val(data[i].id).html(data[i].name));
      } else {
        $('#id_metro').append($('<option></option>').val(data[i].id).html(data[i].name));
      }
    }
$('#id_metro').dropdown('refresh');
$('#id_area').dropdown({
  onChange: function(value){
      console.log(value);
      data = []
      $.ajax({
        url: 'api/profile/metros/' + value,
        type: "GET",
        dataType: 'json',
        data: out,
        async:false,
        success: function (out) {
            for (var d in out){
              data.push(out[d]);
            }
          }
    });
    $('#id_metro').dropdown('clear');
    if (data.length > 0){
        $('.metro_block').show();
        $('#id_metro').empty();
        $('#metro_select > .menu').empty();
        $('#id_metro').append($('<option></option>').val('').html('-------'));
      for (var i in data) {
            $('#id_metro').append($('<option></option>').val(data[i].id).html(data[i].name));
            }
    } else {
      $('.metro_block').hide();
      $('#id_metro').empty();
      $('#metro_select > .menu').empty();
    }
    $('#id_metro').dropdown('refresh');
  },
});
    
    $('#id_issued_date, #id_temp_reg_date, #id_temp_reg_last_date, #id_mb_date, #id_med_exam_date').daterangepicker(
        {
            language: 'ru',
            singleDatePicker: true,
            format: 'DD.MM.YYYY',
            minDate: '01.01.1960',
            maxDate: '01.01.2020',
            showDropdowns: true
        }
    );*/

    
});