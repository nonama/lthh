function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$(document).ready(function() {
	
	$('#id_position, #id_area, #id_regions, #id_area_subscribe').dropdown();
	$('#subscribe_submit').click(function(e){
		e.preventDefault();
		data = {}
		var uid = $('#uid').val();
		var area = $('#id_area_subscribe').val();
		var backreg = $( "#id_area_subscribe option:selected" ).text();
		data = {user : uid, area: area}
		$.ajax({
      		url: '/subscribe',
      		type: 'POST',
      		data: data,
			headers: {
    			'X-CSRFToken': getCookie('csrftoken')
  			},
      		success: function(data) {
				$('#fade-screen').show();
				$('#subs_success_form').show();
				$('body').css('overflow', 'hidden');
				$('#back_reg').html(backreg);
      		},
      		error: function(xhr, status, err) {
        		console.error(status, err.toString());
      		}
   	 	});
  	});
	$('#subs_succ_form_close').on('click', function(){
			$('#fade-screen').hide();
			$('#subs_success_form').hide();
			$('body').css('overflow', 'auto');
	});
	
	$('#vac_choose').click(function(){
		window.location = "/jobs?area=" + $('#vac_choose_reg').val();
	});
});