function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$(document).ready(function(){
    $('#city_select').dropdown({
        onChange: function(){
            var city = $(this).val();
            $('#city_title').html(city);
            $.ajax({
                url: '/come',
                type: "POST",
                headers: {
    			    'X-CSRFToken': getCookie('csrftoken')
  			    },
                data: {'city' : city },
                async:false,
                success: function (data) {
                    $('#office_info').html(data);
                },
                error: function (error) {
                    console.log(error);
                },
            });
        },
    });
});