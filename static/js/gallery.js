$(document).ready(function() {
	$('#grid').mediaBoxes({
           boxesToLoadStart: 55,
           boxesToLoad: 5,
           lazyLoad: true,
           horizontalSpaceBetweenBoxes: 20,
           verticalSpaceBetweenBoxes: 20,
           LoadingWord: 'Загрузка...',
           loadMoreWord: 'Загрузить еще',
           noMoreEntriesWord: 'Нет больше фотографий',
        });
});