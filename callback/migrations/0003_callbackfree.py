# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0016_auto_20161130_1331'),
        ('callback', '0002_callback_phone'),
    ]

    operations = [
        migrations.CreateModel(
            name='CallbackFree',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('requester', models.CharField(max_length=64, null=True, blank=True)),
                ('phone', models.CharField(max_length=20, null=True, blank=True)),
                ('status', models.IntegerField(default=1, blank=True, choices=[(b'1', b'\xd0\x9d\xd0\xbe\xd0\xb2\xd1\x8b\xd0\xb9'), (b'2', b'\xd0\x92 \xd1\x80\xd0\xb0\xd0\xb1\xd0\xbe\xd1\x82\xd0\xb5'), (b'3', b'\xd0\x9e\xd1\x82\xd1\x80\xd0\xb0\xd0\xb1\xd0\xbe\xd1\x82\xd0\xb0\xd0\xbd')])),
                ('comment', models.TextField(null=True, blank=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('area', models.ForeignKey(to='users.Area')),
            ],
        ),
    ]
