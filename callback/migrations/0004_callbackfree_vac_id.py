# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('callback', '0003_callbackfree'),
    ]

    operations = [
        migrations.AddField(
            model_name='callbackfree',
            name='vac_id',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
