# -*- coding: utf-8 -*-
from django import forms
from models import Callback
from django.contrib.auth.models import User



class CallbackForm(forms.ModelForm):
    class Meta:
        model = Callback
        exclude = ('requester', 'created', 'modified', 'status')
        

class CCUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'first_name', 'last_name')