# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from users.models import Area

STATUS = (
    ('1', "Новый"),
    ('2', "В работе"),
    ('3', "Отработан"),
)

class CallbackAdmin(models.Model):
    user = models.ForeignKey(User)
    area = models.ForeignKey(Area)
    email = models.EmailField()
    
    def __unicode__(self):
        return self.user.profile.l_name

class Callback(models.Model):
    requester = models.ForeignKey(User)
    area = models.ForeignKey(Area)
    phone = models.CharField(null=True, blank=True, max_length=20)
    status = models.IntegerField(blank=True, choices=STATUS, default=1)
    comment = models.TextField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        return self.requester.profile.l_name

class CallbackFree(models.Model):
    requester = models.CharField(null=True, blank=True, max_length=64)
    area = models.ForeignKey(Area)
    phone = models.CharField(null=True, blank=True, max_length=20)
    status = models.IntegerField(blank=True, choices=STATUS, default=1)
    comment = models.TextField(blank=True, null=True)
    vac_id = models.IntegerField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        return self.requester