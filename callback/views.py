from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from models import Callback
from users.models import Area, Profile
from forms import CallbackForm, CCUserForm
from decorators import group_required


def callback(request):
    #u = User.objects.get(pk = request.user.id)
    reg = Area.objects.all()
    if request.method == 'POST':
        call_form = CallbackForm(request.POST)
        if call_form.is_valid():
            call = call_form.save(commit=False)
            call.requester = request.user
            call.status = 1
            call.save()
            return HttpResponseRedirect('callback/success')
    else:
        call_form = CallbackForm(request.POST)
    return render(request, 'callback.html', {'call_form' : call_form, 'off' : reg})

def cb_succ(request):
    return render(request, 'callback_success.html', {})
    
@group_required('CC')
def cb_admin(request):
    u = User.objects.get(pk = request.user.id)
    cc_tasks = Callback.objects.all()#filter(area = u.profile.area)
    is_cc_admin = request.user.groups.filter(name__in=['CC_ADMINS', 'ADMINS']).exists()
    is_cc = request.user.groups.filter(name='CC').exists()
    cc_users = request.user.groups.filter(name__in=['CC_ADMINS', 'CC'])
    #cc_tasks = Callback.objects.all()
    if request.method == "POST":
        cc_form = CCUserForm(request.POST)
        if cc_form.is_valid():
            new_user = User.objects.create_user(**cc_form.cleaned_data)
            new_user.save()
            g = Group.objects.get(name='CC')
            g.user_set.add(new_user)
            up = Profile.objects.filter(user = new_user)
            up.update(area = request.user.profile.area)
            up.update(filled = True)
            # redirect, or however you want to get to the main view
            return HttpResponseRedirect('cb_admin')
    else:
        cc_form = CCUserForm()
    return render(request, 'callback_admin.html', {'cc_queue' : cc_tasks, 'is_cc' : is_cc, 'is_cc_admin' : is_cc_admin, 'cc_form' : cc_form, 'cc_users' : cc_users })

def masterzone_calls(request):
    calls = Callback.objects.all().order_by('-created')
    return render(request, 'admin/calls.html', {'calls' : calls })

def masterzone_calls_take(request, cid):
    Callback.objects.filter(id=cid).update(status=2)
    calls = Callback.objects.all().order_by('-created')
    return render(request, 'admin/calls.html', {'calls' : calls })