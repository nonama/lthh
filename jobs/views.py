# -*- coding: utf-8 -*-
from datetime import datetime
import time
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from models import Vacancy, Shedule, Position, Reply, ReplyStatus, Notification
from users.forms import ProfileForm
from forms import ReplyForm
from users.models import Area, Profile, Citizenship
from filters import JobFilter
import misc
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from functions import user_age, send_warn, send_success
from dbmail import send_db_sms


def all(request):
    age = user_age(request)
    new = ReplyStatus.objects.get(id=0)
    jobs = JobFilter(request.GET, queryset = Vacancy.objects.filter(active=True).order_by('-created'))
    replies = Reply.objects.filter(user = request.user.id).exclude(active = False)
    if request.method == 'POST':
        if request.user.profile.citizenship.id == 643 and age >= 18:
            if replies.count >= 1:
                Reply.objects.filter(user=request.user.id, active=True).update(active=False)
            reply_form = ReplyForm(request.POST)
            if reply_form.is_valid():
                reply = reply_form.save(commit=False)
                reply.user = request.user
                reply.status = new
                reply.save()
                if reply.vacancy.position.id == 292:
                    send_success_custom(request, reply)
                else:
                    send_success(request, reply)
                return HttpResponseRedirect('/replies')
        else:
            time.sleep(10)
            send_warn(request)
            return HttpResponseRedirect('/notifications')
    else:
        reply_form = ReplyForm()
        profile_form = ProfileForm()
    return render(request, 'jobs.html', { 'reply_form' : reply_form, 'replies' : replies, 'jobs' : jobs, 'profile_form' : profile_form })

def job(request, job_id):
    age = user_age(request)
    new = ReplyStatus.objects.get(id=0)
    job = Vacancy.objects.get(id=job_id)
    replies = Reply.objects.filter(user = request.user.id).exclude(active = False)
    if request.method == 'POST':
        if request.user.profile.citizenship.id == 643 and age >= 18:
            reply_form = ReplyForm(request.POST)
            if reply_form.is_valid():
                reply = reply_form.save(commit=False)
                reply.user = request.user
                reply.status = new
                reply.save()
                send_success(request, reply)
                return HttpResponseRedirect('/replies')
        else:
            send_warn(request)
            return HttpResponseRedirect('/notifications')
    else:
        reply_form = ReplyForm()
        profile_form = ProfileForm()
    return render(request, 'job.html', { 'job': job, 'reply_form' : reply_form, 'replies' : replies, 'profile_form' : profile_form })
    
def replies(request):
    replies = Reply.objects.filter(user = request.user.id).order_by('-modified')
    return render(request, 'replies.html', { 'replies': replies,})

def reply_delete(request, rid):
    Reply.objects.filter(id=rid).update(active=False)
    return HttpResponseRedirect('/jobs')

def reply_delete_all(request):
    Reply.objects.filter(user=request.user.id, active=True).update(active=False)
    return HttpResponseRedirect('/jobs')

def notifications(request):
    notifications = Notification.objects.filter(user = request.user.id).order_by('-created')
    response = render(request, 'notifications.html', { 'notifications': notifications,})
    notifications.update(new=False)
    return response