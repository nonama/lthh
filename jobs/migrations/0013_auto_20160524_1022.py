# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0012_auto_20160518_0835'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='position',
            options={'ordering': ('position',)},
        ),
        migrations.AlterModelOptions(
            name='shedule',
            options={'ordering': ('shedule',)},
        ),
    ]
