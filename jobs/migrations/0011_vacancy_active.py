# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-05-16 12:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0010_auto_20160512_2113'),
    ]

    operations = [
        migrations.AddField(
            model_name='vacancy',
            name='active',
            field=models.BooleanField(default=True),
        ),
    ]
