# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0018_vacancy_rotational'),
    ]

    operations = [
        migrations.AddField(
            model_name='position',
            name='image',
            field=models.ImageField(default=b'', upload_to=b'static/img/positions/', blank=True),
        ),
    ]
