# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0016_reply_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='notification',
            name='new',
            field=models.BooleanField(default=True),
        ),
    ]
