# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0013_auto_20160524_1022'),
    ]

    operations = [
        migrations.AddField(
            model_name='vacancy',
            name='urgent',
            field=models.BooleanField(default=False),
        ),
    ]
