# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0015_auto_20160623_1128'),
    ]

    operations = [
        migrations.AddField(
            model_name='reply',
            name='active',
            field=models.BooleanField(default=True),
        ),
    ]
