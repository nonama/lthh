# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0017_notification_new'),
    ]

    operations = [
        migrations.AddField(
            model_name='vacancy',
            name='rotational',
            field=models.BooleanField(default=False),
        ),
    ]
