# -*- coding: utf-8 -*-
from django import forms
from models import Vacancy
import django_filters

class JobFilter(django_filters.FilterSet):
    rotational = django_filters.BooleanFilter(widget = forms.CheckboxInput())
    class Meta:
        model = Vacancy
        fields = {'area': ['exact'],
                  'rotational': ['exact'],
                  'position': ['exact'],
                  #'min_rate': ['lte'],
                  'max_rate': ['gte'],
                  'shedule': ['exact'],
                 }
        
class JobMainFilter(django_filters.FilterSet):
    class Meta:
        model = Vacancy
        fields = {'area': ['exact'],
                  'position': ['exact'],
                 }