from django import template
from users.models import Area
register = template.Library()


@register.inclusion_tag("regions_list.html")
def regions():
    regions = Area.objects.all()
    return {'regions' : regions }