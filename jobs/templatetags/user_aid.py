from django import template
from geoip.models import AreaByIP, Cities
register = template.Library()

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

@register.simple_tag(name="user_aid", takes_context=True)
def user_aid(context):
    request = context['request']
    out = 1
    try:
        current_user_aid = request.user.profile.area.id
        out = current_user_aid
    except:
        current_user_aid = 0
    if not current_user_aid:
        ips = get_client_ip(request).split('.')
        ip = ips[0] + ips[1] + ips[2]
        try:
            aid = AreaByIP.objects.get(ip = ip).aid
        except:
            aid = '581179'
        out = Cities.objects.get(aid = aid).rid
    return out