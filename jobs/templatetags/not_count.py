from django import template
from jobs.models import Notification
register = template.Library()


@register.simple_tag(name="not_count", takes_context=True)
def not_count(context):
    request = context['request']
    count = Notification.objects.filter(user = request.user.id, new = True).count()
    if count > 0:
        return ' <div class="ui orange circular medium label">' + str(count) + '</div>'
    else:
        return ''