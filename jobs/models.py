# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from users.models import Area


class Position(models.Model):
    id = models.IntegerField(unique=True, null=False, primary_key=True, default = 0)
    position = models.CharField(blank=True, max_length=255)
    image = models.ImageField(upload_to='static/img/positions/', blank = True, default='')
    
    def __unicode__(self):
        return self.position
    
    class Meta:
        ordering = ('position',)


class Shedule(models.Model):
    id = models.IntegerField(unique=True, null=False, primary_key=True, default = 0)
    shedule = models.CharField(blank=True, max_length=255)
    
    def __unicode__(self):
        return self.shedule
        
    class Meta:
        ordering = ('shedule',)


class Vacancy(models.Model):
    id = models.IntegerField(unique=True, null=False, primary_key=True, default = 0)
    position = models.ForeignKey(Position)
    description = models.TextField(blank=True)
    area = models.ForeignKey(Area)
    max_rate = models.PositiveIntegerField()
    min_rate = models.PositiveIntegerField()
    shedule = models.ManyToManyField(Shedule)
    urgent = models.BooleanField(default=False)
    rotational = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=True)
    
    def __unicode__(self):
        return unicode(self.position)


class ReplyStatus(models.Model):
    id = models.IntegerField(unique=True, null=False, primary_key=True, default = 0)
    status = models.CharField(max_length=255)
    
    def __unicode__(self):
        return unicode(self.status)


class Reply(models.Model):
    id = models.AutoField(primary_key=True)
    vacancy = models.ForeignKey(Vacancy)
    user = models.ForeignKey(User)
    status = models.ForeignKey(ReplyStatus, to_field='id')
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)
    
    def __unicode__(self):
        return unicode(self.vacancy)


class Notification(models.Model):
    user = models.ForeignKey(User)
    text = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    new = models.BooleanField(default=True)
    
    def __unicode__(self):
        return unicode(self.text)