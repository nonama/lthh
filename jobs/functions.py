# -*- coding: utf-8 -*-
import misc
from dbmail import send_db_sms, send_db_mail
from models import Notification
from datetime import datetime

def user_age(request):
    user = request.user
    try:
        date = user.profile.bd_month + "/" + user.profile.bd_day + "/" + user.profile.bd_year
        user_age = ((datetime.today() - datetime.strptime(date, '%m/%d/%Y')).days/365)
    except:
        user_age = 0
    return user_age

def send_warn(request):
    user = request.user
    warning = Notification(user = user, text = misc.warning_text)
    warning.save()
    user_phone = user.profile.phone
    recipient = str(user_phone[0:2] + user_phone[4:7] + user_phone[9:12] + user_phone[13:15] + user_phone[16:18])
    send_db_sms(slug="warn_sms", recipient = recipient, use_celery=False)
    send_db_mail("warn_mail", user.email, use_celery = False)

def send_success(request, reply):
    user = request.user
    success = Notification(user = user, text = "Ваша заявка поступила в обработку.")
    success.save()
    user_phone = user.profile.phone
    recipient = str(user_phone[0:2] + user_phone[4:7] + user_phone[9:12] + user_phone[13:15] + user_phone[16:18])
    send_db_sms("rep_sms", recipient, use_celery = False)
    send_db_mail("rep_email", user.email, use_celery = False)

def send_success_custom(request, reply):
    user = request.user
    success = Notification(user = user, text = "В ближайшее время с Вами свяжется специалист!")
    success.save()
    vacancy = { 'vacancy' : str(reply.vacancy) }
    profile = {'profile' : user.profile }
    user_phone = user.profile.phone
    recipient = str(user_phone[0:2] + user_phone[4:7] + user_phone[9:12] + user_phone[13:15] + user_phone[16:18])
    send_db_sms("reply_sms", recipient, use_celery = False)
    send_db_mail("reply_mail", user.email, use_celery = False)
    send_db_mail("centr_mail", 'hr@rvzr.ru', profile, use_celery = False)