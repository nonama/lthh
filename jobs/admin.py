# -*- coding: utf-8 -*-
from django.contrib import admin
from jobs.models import Position, Vacancy, Shedule, ReplyStatus, Reply, Notification

admin.site.register(Position)
admin.site.register(Vacancy)
admin.site.register(Shedule)
admin.site.register(ReplyStatus)
admin.site.register(Reply)
admin.site.register(Notification)